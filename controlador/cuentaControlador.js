'use strict';
var cuenta = require('../modelos/cuenta');
class cuentaControlador {

    /**
     * Clase que permite iniciar sessiom
     * @param {type} req objeto peticion
     * @param {type} res onjetorespuesta
     * @returns {undefined}
     */
    iniciar_sesion(req, res) {
        var datos = {
            correo: req.body.correo,
            clave: req.body.clave
        }
        console.log(datos);
        // Medico.save().then(function(result) {//
        cuenta.getJoin({ medico: true }).filter({ correo: datos.correo }).run().then(function(result) {
            if (result.length > 0) {
                var resultado = result[0];
                console.log(resultado);
                if (resultado.clave === datos.clave) {
                    req.session.cuenta = { external: resultado.medico.external_id, id: resultado.id_medico, usuario: resultado.medico.apellidos + " " + resultado.medico.nombres };
                    // res.send(req.session.resultado);
                    res.redirect("/principal");
                    // res.render('principal', { title: 'Sistema Medico', session: true, fragmento: "fragmentos/tablaPaciente" });
                } else {
                    res.locals.error = req.flash('error', 'Sus credenciales no son los correctos');
                    res.redirect("/principal");
                }
            } else {
                res.locals.error = req.flash('error', 'Sus credenciales no son los correctos');
                res.redirect("/principal");
            }

        }).catch(function(error) {
            res.send(error);
        });
    }
    cerrar_sesion(req, res) {
        req.session.destroy();
        res.redirect("/principal");
    }
}
module.exports = cuentaControlador;