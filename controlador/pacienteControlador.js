'use strict';
var paciente = require('../modelos/paciente');
var historial = require('../modelos/historial');
class pacienteControlador {

    guardar(req, res) {
        var datos = {
            cedula: req.body.txtcedula,
            apellidos: req.body.txtapellidos,
            nombres: req.body.txtnombres,
            fecha_nac: req.body.fecha,
            edad: req.body.txtedad,
            direccion: req.body.txtdir,
        };

        console.log(datos);
        var datosH = {
            habitos: req.body.txthabitos,
            nro_historia: req.body.txtnrohistorial,
            contacto: req.body.txtcontacto,
            enfermedades: req.body.txtenfer,
            enfermedades_hereditarias: req.body.txtenf_h
        };
        console.log(datosH);
        var Paciente = new paciente(datos);
        var Historial = new historial(datosH);

        Paciente.historial = Historial;
        //then es una promesa que si no hay erro se guarda
        // Medico.save().then(function(result) {//
        Paciente.saveAll({ historial: true }).then(function(result) {
            // req.flash('info', 'Paciente registrado!');
            res.redirect("/Paciente");
            // res.render('principal', { title: 'Sistema Medico', session: false });
        }).error(function(error) {
            req.flash('error', 'No se pudo registrar!');
            res.redirect("/Paciente");
        });
    }
    cargarPacientes(req, res) {
        paciente.getJoin({ historial: true }).run().then(function(result) {
            //fotma mas larga//NHIS-?
            // historial.count().then(function(cant) {
            //     var nro = "NHIS-" + (cant + 1);
            // }).error(function(error) {
            //     res.locals.error = req.flash('error', 'Hubo un error!');
            //     res.redirect("/principal");
            // });
            //fializa forma larga
            var nro = "NHIS-" + (result.length + 1);
            console.log(nro);
            res.render('principal', { title: 'Sistema Medico', session: true, fragmento: "fragmentos/tablaPaciente", usuario: req.session.cuenta.usuario, listado: result, msg: { error: req.flash('error'), success: req.flash('success') }, nro: nro, active: 'paciente' });
        }).catch(function(error) {
            res.locals.error = req.flash('error', 'Hubo un error!');
            res.redirect("/principal");
        });
    }

    visualizar_modificar(req, res) {
        var external = req.params.external;
        console.log("external" + external);
        paciente.getJoin({ historial: true }).filter({ external_id: external }).then(function(result) {
            if (result.length > 0) {
                var paciente = result[0];
                res.render('principal', { title: 'Sistema Medico', session: true, fragmento: "fragmentos/modificarPaciente", usuario: req.session.cuenta.usuario, paci: paciente, msg: { error: req.flash('error') }, active: 'paciente' });
            } else {
                req.flash('error', 'No se pudo encontrar el registro!');
                res.redirect("/Paciente");
            }
        }).error(function(error) {
            req.flash('error', 'No se pudo encontrar el registro!');
            res.redirect("/Paciente");
        });
    }

    modificar(req, res) {
        paciente.getJoin({ historial: true }).filter({ external_id: req.body.external }).then(function(result) {
            if (result.length > 0) {
                var paciente = result[0];
                paciente.apellidos = req.body.txtapellidos;
                paciente.nombres = req.body.txtnombres;
                paciente.fecha_nac = req.body.fecha;
                paciente.edad = req.body.txtedad;
                paciente.direccion = req.body.txtdir;

                paciente.historial.habitos = req.body.txthabitos;
                paciente.historial.contacto = req.body.txtcontacto;
                paciente.historial.enfermedades = req.body.txtenfer;
                paciente.historial.enfermedades_hereditarias = req.body.txtenf_h;

                paciente.saveAll({ historial: true }).then(function(resultado) {
                    req.flash('success', 'Paciente modificado correctamente!');
                    res.redirect("/Paciente");
                }).error(function(error) {
                    req.flash('error', 'No se pudo modificar paciente registro!');
                    res.redirect("/modificar/:" + req.body.external_id);
                });
            } else {
                req.flash('error', 'No se pudo modificar paciente registro!');
                res.redirect("/modificar/:" + req.body.external_id);
            }



        }).error(function(error) {
            req.flash('error', 'No se pudo Actualizar!');
            res.redirect("/Paciente");
        });
    }
    buscar(req, res) {
        //se recibe los datos del servicio web
        var criterio = req.query.criterio;
        var texto = req.query.texto;
        var data = {};
        if (criterio === 'todos') {
            paciente.getJoin({ historial: true }).then(function(result) {
                result.forEach(function(item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro_historia: item.historial.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function() {

            });
        } else
        if (criterio === 'cedula') {
            console.log(texto);
            paciente.getJoin({ historial: true }).filter({ cedula: texto }).then(function(result) {
                result.forEach(function(item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro_historia: item.historial.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function() {

            });
        } else
        if (criterio === 'nro_historial') {
            paciente.getJoin({ historial: true }).filter(function(datos) {
                return datos('historial')('nro_historia').eq(texto);
            }).then(function(result) {
                result.forEach(function(item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro_historia: item.historial.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function() {

            });
        } else
        if (criterio === 'apellidos') {
            paciente.getJoin({ historial: true }).filter(function(datos) {
                return datos('apellidos').match(texto);
            }).then(function(result) {
                result.forEach(function(item, index) {
                    data[index] = {
                        cedula: item.cedula,
                        paciente: item.apellidos + " " + item.nombres,
                        nro_historia: item.historial.nro_historia,
                        external: item.external_id
                    };
                });
                res.json(data);
            }).error(function() {

            });
        }
    }
}


module.exports = pacienteControlador;