'use strict';
var medico = require('../modelos/medico');
var cuenta = require('../modelos/cuenta');
class medicoControlador {

    iniciar(req, res) {
        var datos = {
            correo: req.body.correo,
            clave: req.body.clave
        }
        console.log(datos);
        // Medico.save().then(function(result) {//
        cuenta.filter({ correo: datos.correo }).run().then(function(result) {
            // console.log(result[0].clave);
            // console.log(datos.clave);
            // res.send(result);
            if (result[0].clave === datos.clave) {
                console.log(result[0].correo);
                console.log(result[0].external_id);
                // req.session.user = "Yukas";
                // req.session.external = result[0].external_id;
                // console.log(req.session.user + " *********** " + req.session.external);
                // req.session.cookie.expires = false;
                // res.redirect("/");
                res.render('principal', { title: 'Sistema Medico', session: true, fragmento: "fragmentos/tablaPaciente" });
            } else {
                res.send("errroroeorooeor");
            }
        }).catch(function(error) {
            res.send(error);
        });

        //     User.get(id)
        // .then(function(user) {
        //     console.log(user);
        // })
        // .catch(function(error) {
        //     console.log(error);
        // });
    }



    guardar(req, res) {
        var datos = {
            cedula: req.body.txtcedula,
            apellidos: req.body.txtapellido,
            nombres: req.body.txtnombre,
            fecha_nac: req.body.txtfechaNac,
            edad: req.body.txtedad,
            nro_registro: req.body.txtregistro,
            especialidad: req.body.especialidad
        };
        console.log(datos);
        var datosC = {
            correo: req.body.txtcorreo,
            clave: req.body.txtclave
        };
        console.log(datosC);
        var Medico = new medico(datos);
        var Cuenta = new cuenta(datosC);

        Medico.cuenta = Cuenta;
        //then es una promesa que si no hay erro se guarda
        // Medico.save().then(function(result) {//
        Medico.saveAll({ cuenta: true }).then(function(result) {
            // res.send(result);

            res.render('principal', { title: 'Sistema Medico', session: false });
        }).error(function(error) {
            res.send(error);
        });
    }
}
module.exports = medicoControlador;