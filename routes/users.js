var express = require('express');
var router = express.Router();

//se llama al controlador paciente de la carpeta controlador
var Paciente = require('../controlador/pacienteControlador');
var paciente = new Paciente();

//para verificar si esta iniciado session
function verificar_inicio(req) {
    return (req.session != undefined && req.session.cuenta != undefined);
}
var auth = function(req, res, next) {
    //para roles se nesecita otro verificador
    if (verificar_inicio(req)) {
        next();
    } else {
        res.json({ msg: 'no puedes hacer eso', cod: 403 });
        // req.flash('error', 'Debes de iniciar sesion primero');
        // res.redirect("/principal");
    }
}



/* GET users listing. */
router.get('/', auth, function(req, res, next) {
    res.send('respond with a resource');
});

//para bsucar paciente
router.get('/buscar_paciente', paciente.buscar);

module.exports = router;