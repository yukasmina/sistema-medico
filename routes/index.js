var express = require('express');
var router = express.Router();
//se llama al controlador cuenta de la carpeta controlador
var Cuenta = require('../controlador/cuentaControlador');
var cuenta = new Cuenta();
//se llama al controlador medico de la carpeta controlador
var Medico = require('../controlador/medicoControlador');
var medico = new Medico();
//se llama al controlador paciente de la carpeta controlador
var Paciente = require('../controlador/pacienteControlador');
var paciente = new Paciente();

//llamando a controlador consulta
var Consulta = require('../controlador/consultaControlador');
var consulta = new Consulta();

//para verificar si esta iniciado session
function verificar_inicio(req) {
    return (req.session != undefined && req.session.cuenta != undefined);
}
var auth = function(req, res, next) {
    //para roles se nesecita otro verificador
    if (verificar_inicio(req)) {
        next();
    } else {
        req.flash('error', 'Debes de iniciar sesion primero');
        res.redirect("/principal");
    }
}


// /* GET home page. */
// router.get('/', function(req, res, next) {
//     // req.flash('success_msg', 'Gracias por crear tu cuenta, ahora estas autentificado.');
//     req.flash('error', 'Woops, looks like that username and password are incorrect.');
//     res.render('index', { title: 'Bienvenido', nombre: 'Angel Minga', msg: { error: req.flash('error'), success: req.flash('success') } });
// });

/* GET home pagina principal. */
router.get('/principal', function(req, res, next) {
    if (req.session != undefined && req.session.cuenta != undefined) {
        // router.get('/cargar', paciente.cargarPacientes);
        // console.log(paciente.cargarPacientes);
        res.render('principal', { title: 'Sistema Medico', session: true, fragmento: "fragmentos/bienvenido", usuario: req.session.cuenta.usuario, active: 'home' });
    } else {
        res.render('principal', { title: 'Sistema Medico', session: false, msg: { error: req.flash('error'), success: req.flash('success') } });
    }
});

//iniciar sesion medico
router.post('/inicio_sesion', cuenta.iniciar_sesion);

//cerrar sesion
router.get('/cerrar_sesion', auth, cuenta.cerrar_sesion);


/* GET home pagina registro. */
router.get('/registro', function(req, res, next) {
    // req.flash('mensaje', 'Flash is back!');
    // req.flash('info', 'Flash is back!');
    res.render('principal', { title: 'Sistema Medico', session: true, fragmento: "fragmentos/registromedico", usuario: " " });
});

//cargar paciente en las tablas
router.get('/Paciente', auth, paciente.cargarPacientes);
/**
 * Guardar paciente e historia en la base de datos
 */
router.post('/registroPaciente', auth, paciente.guardar);
router.get('/modificar/:external', auth, paciente.visualizar_modificar);
router.post('/actualizar', auth, paciente.modificar);

/**
 * Guardar medico en la db
 */
router.post('/registrar', medico.guardar);

//enlaces para consulta
router.get('/consulta', auth, consulta.cargarPantalla);



module.exports = router;