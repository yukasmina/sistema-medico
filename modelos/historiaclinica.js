var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var HistoriaClinica = thinky.createModel("HistoriaClinica", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    diagnostico: type.string(),
    fecha: type.date(),
    motivo: type.string(),
    receta: type.string(),
    createdAt: type.date().default(r.now()),
    id_medico: type.string(),
    id_historial: type.string()
});
module.exports = HistoriaClinica;

var Medico = require("./medico");
HistoriaClinica.belongsTo(Medico, "medico", "id_medico", "id");

var Historial = require("./historial");
HistoriaClinica.belongsTo(Historial, "historial", "id_historial", "id");