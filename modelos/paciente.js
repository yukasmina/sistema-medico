var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Paciente = thinky.createModel("Paciente", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    cedula: type.string(),
    apellidos: type.string(),
    nombres: type.string(),
    direccion: type.string(),
    fecha_nac: type.date(),
    edad: type.number(),
    createdAt: type.date().default(r.now())
});
module.exports = Paciente;
var Historial = require("./historial");
Paciente.hasOne(Historial, "historial", "id", "id_paciente");