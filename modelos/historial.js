var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Historial = thinky.createModel("Historial", {
    id: type.string(),
    external_id: type.string().default(r.uuid()),
    nro_historia: type.string(),
    habitos: type.string(),
    contacto: type.string(),
    enfermedades: type.string(),
    enfermedades_hederitarias: type.string(),
    createdAt: type.date().default(r.now()),
    id_paciente: type.string()
});
module.exports = Historial;
var Paciente = require("./paciente");
Historial.belongsTo(Paciente, "paciente", "id_paciente", "id");

//realacion historial historialclinico
var HistoriaClinica = require("./historiaclinica");
Historial.hasMany(HistoriaClinica, "historiaclinica", "id", "id_historial");